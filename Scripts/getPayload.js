System.log("Start");
 
//The payload is grabbed by the inputProperties
System.log(JSON.stringify(inputProperties, null, 2));
 
var jsontxt = JSON.stringify(inputProperties, null, 2);
var json = JSON.parse(jsontxt);
 
//Extract Properties 
vcUuid = json.customProperties.vcUuid
System.log("vcUuid = " + vcUuid);
vmName = json.resourceNames[0];
System.log("vmName = " + vmName);
var domainName = json.customProperties.domainName;
System.log("Domain Name = " + domainName);
newVMName = vmName + '.' + domainName;

//****************************************************
 
//Loop through the Execution contect
for (p in System.getContext().parameterNames()) {
    System.log(System.getContext().parameterNames()[p]+": "+System.getContext().getParameter(System.getContext().parameterNames()[p]));
} 
 
//To get a specific property 
//System.log(System.getContext().getParameter("__metadata_userName"));

if (vcUuid) {
  var sdkConnection = VcPlugin.findSdkConnectionForUUID(vcUuid);
  vcVM = sdkConnection.searchIndex.findByUuid(null, inputProperties["externalIds"][0], true, true);
}
//return vCenterVM;
System.log("VM hostname: " + vcVM.hostName)
 
System.log("Stop");