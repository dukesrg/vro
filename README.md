# vRealize Orchestrator


# ������ ������� ��������� � �����������
- [����������� ���� �� ����������](Workflows/Library/EB Network Register with vRA plugin) - ������� ������� ��� ����������� on-demand ���� � ������� ������� ����� �������� Event Broker

# �������� ������

- [VMware vRealize Orchestrator Plug-in for vRealize Automation 8.3](https://marketplace.cloud.vmware.com/services/details/vmware-vrealize-orchestrator-plug-in-for-vrealize-automation-1?slug=true)
- [vRealize Automation 8.4 Extensibility Migration Guide Samples](https://code.vmware.com/samples/7620/vrealize-automation-8-4-extensibility-migration-guide-samples)
- [vRealize Automation 8.3 Extensibility Migration Guide Samples](https://code.vmware.com/samples/7574/vrealize-automation-8-3-extensibility-migration-guide-samples)
- [vRealize Automation 8.2 Extensibility Migration Guide Samples](https://code.vmware.com/samples/7469/vrealize-automation-8-2-extensibility-migration-guide-samples)
